
install(FILES
        biofilm.hh
        biosub.hh
        biosusp.hh
        brine_varSalinity.hh
        ca.hh
        calcite.hh
        cl.hh
        co3.hh
        hco3.hh
        hPlus.hh
        na.hh
        nh3.hh
        nh4.hh
        oh.hh
        urea.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/components)
