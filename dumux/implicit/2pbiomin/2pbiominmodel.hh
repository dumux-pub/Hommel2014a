// $Id: 2p2cmodel.hh 5093 2011-01-23 18:05:49Z claude $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
* \file
*
* \brief Adaption of the BOX scheme to the two-phase biomineralisation flow model.
*/

#ifndef DUMUX_2PBIOMIN_MODEL_HH
#define DUMUX_2PBIOMIN_MODEL_HH
#define VELOCITY_OUTPUT

#include "dumux/implicit/2pncmin/2pncminproperties.hh"
#include "2pbiominlocalresidual.hh"
#include "dumux/implicit/2pncmin/2pncminlocalresidual.hh"
//#include "dumux/implicit/2pncmin/2pncminproblem.hh"
#include "dumux/implicit/2pncmin/2pncminpropertydefaults.hh"
//#include "dumux/implicit/2pncmin/2pbiominpropertydefaults.hh"
#include "dumux/implicit/2pncmin/2pncminmodel.hh"

#include <boost/format.hpp>
#include <cmath>

namespace Dumux
{
/*!
 * \ingroup TwoPNCMinModelBoxModels
 * \defgroup TwoPBioMinModel Two-phase biomineralisation box model
 */

/*!
 * \ingroup TwoPBioMinModel
 * \brief Adaption of the BOX scheme to the two-phase biomineralisation flow model.
 *
 * This model implements two-phase n-component mineralisation flow of two compressible and
 * partially miscible fluids \f$\alpha \in \{ w, n \}\f$ composed of the n components
 * \f$\kappa \in \{ w, a \}\f$. The standard multiphase Darcy
 * approach is used as the equation for the conservation of momentum:
 * \f[
 v_\alpha = - \frac{k_{r\alpha}}{\mu_\alpha} \mbox{\bf K}
 \left(\text{grad}\, p_\alpha - \varrho_{\alpha} \mbox{\bf g} \right)
 * \f]
 *
 * By inserting this into the equations for the conservation of the
 * components, one gets one transport equation for each component
 * \f{eqnarray}
 && \phi \frac{\partial (\sum_\alpha \varrho_\alpha X_\alpha^\kappa S_\alpha )}
 {\partial t}
 - \sum_\alpha  \text{div} \left\{ \varrho_\alpha X_\alpha^\kappa
 \frac{k_{r\alpha}}{\mu_\alpha} \mbox{\bf K}
 (\text{grad}\, p_\alpha - \varrho_{\alpha}  \mbox{\bf g}) \right\}
 \nonumber \\ \nonumber \\
    &-& \sum_\alpha \text{div} \left\{{\bf D_{\alpha, pm}^\kappa} \varrho_{\alpha} \text{grad}\, X^\kappa_{\alpha} \right\}
 - \sum_\alpha q_\alpha^\kappa = 0 \qquad \kappa \in \{w, a\} \, ,
 \alpha \in \{w, g\}
 \f}
 *
 * This is discretized using a fully-coupled vertex
 * centered finite volume (box) scheme as spatial and
 * the implicit Euler method as temporal discretization.
 *
 * By using constitutive relations for the capillary pressure \f$p_c =
 * p_n - p_w\f$ and relative permeability \f$k_{r\alpha}\f$ and taking
 * advantage of the fact that \f$S_w + S_n = 1\f$ and \f$X^\kappa_w + X^\kappa_n = 1\f$, the number of
 * unknowns can be reduced to two.
 * The used primary variables are, like in the two-phase model, either \f$p_w\f$ and \f$S_n\f$
 * or \f$p_n\f$ and \f$S_w\f$. The formulation which ought to be used can be
 * specified by setting the <tt>Formulation</tt> property to either
 * TwoPTwoCIndices::pWsN or TwoPTwoCIndices::pNsW. By
 * default, the model uses \f$p_w\f$ and \f$S_n\f$.
 * Moreover, the second primary variable depends on the phase state, since a
 * primary variable switch is included. The phase state is stored for all nodes
 * of the system. Following cases can be distinguished:
 * <ul>
 *  <li> Both phases are present: The saturation is used (either \f$S_n\f$ or \f$S_w\f$, dependent on the chosen <tt>Formulation</tt>),
 *      as long as \f$ 0 < S_\alpha < 1\f$</li>.
 *  <li> Only wetting phase is present: The mass fraction of, e.g., air in the wetting phase \f$X^a_w\f$ is used,
 *      as long as the maximum mass fraction is not exceeded (\f$X^a_w<X^a_{w,max}\f$)</li>
 *  <li> Only non-wetting phase is present: The mass fraction of, e.g., water in the non-wetting phase, \f$X^w_n\f$, is used,
 *      as long as the maximum mass fraction is not exceeded (\f$X^w_n<X^w_{n,max}\f$)</li>
 * </ul>
 */

template<class TypeTag>
class TwoPBioMinModel: public TwoPNCMinModel<TypeTag>
{
	typedef TwoPBioMinModel<TypeTag> ThisType;
    typedef TwoPNCMinModel<TypeTag> ParentType;
    typedef ImplicitModel<TypeTag> BaseClassType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VertexMapper) VertexMapper;
    typedef typename GET_PROP_TYPE(TypeTag, ElementMapper) ElementMapper;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases = GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numSecComponents = GET_PROP_VALUE(TypeTag, NumSecComponents),

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        UreaIdx	= FluidSystem::UreaIdx,
        CaIdx = FluidSystem::CaIdx,

        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,
        formulation = GET_PROP_VALUE(TypeTag, Formulation)
    };

    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;

    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename GridView::ctype CoordScalar;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;
    typedef Dune::FieldVector<Scalar, numPhases> PhasesVector;

//     static constexpr Scalar ImplicitMobilityUpwindWeight =
//             GET_PROP_VALUE(TypeTag, ImplicitMobilityUpwindWeight);

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
        enum { dofCodim = isBox ? dim : 0 };

public:
    /*!
     * \copydoc 2pncMin::init
     */
    void init(Problem &problem)
    {
        ParentType::init(problem);
    	try
	{
    		plausibilityTolerance_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, PlausibilityTolerance);
    		pHMax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, pHMax);
    		pHMin_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, pHMin);
		massUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
	}

	catch (Dumux::ParameterException &e) {
	    std::cerr << e << ". Abort!\n";
	    exit(1) ;
	}
        unsigned numDofs = this->numDofs();

        staticDat_.resize(numDofs);

        setSwitched_(false);

        // check, if velocity output can be used (works only for cubes so far)
        velocityOutput_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, AddVelocity);
        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator elemEndIt = this->gridView_().template end<0>();
        for (; elemIt != elemEndIt; ++elemIt)
        {
            if (elemIt->geometry().type().isCube() == false){
                velocityOutput_ = false;
            }

            if (!isBox) // i.e. cell-centered discretization
            {
                velocityOutput_ = false;

                int globalIdx = this->dofMapper().map(*elemIt);
                const GlobalPosition &globalPos = elemIt->geometry().center();

                // initialize phase presence
                staticDat_[globalIdx].phasePresence
                    = this->problem_().initialPhasePresence(*(this->gridView_().template begin<dim>()),
                                                            globalIdx, globalPos);
                staticDat_[globalIdx].wasSwitched = false;

                staticDat_[globalIdx].oldPhasePresence
                    = staticDat_[globalIdx].phasePresence;
            }
        }

        if (velocityOutput_ != GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, AddVelocity))
            std::cout << "ATTENTION: Velocity output only works for cubes and is set to false for simplices\n";

        if (isBox) // i.e. vertex-centered discretization
        {
            VertexIterator vIt = this->gridView_().template begin<dim> ();
            const VertexIterator &vEndIt = this->gridView_().template end<dim> ();
            for (; vIt != vEndIt; ++vIt)
            {
                int globalIdx = this->dofMapper().map(*vIt);
                const GlobalPosition &globalPos = vIt->geometry().corner(0);

                // initialize phase presence
                staticDat_[globalIdx].phasePresence
                    = this->problem_().initialPhasePresence(*vIt, globalIdx,
                                                        globalPos);
                staticDat_[globalIdx].wasSwitched = false;

                staticDat_[globalIdx].oldPhasePresence
                    = staticDat_[globalIdx].phasePresence;
            }
        }
    }

    /*!
     * \copydoc 2pncMin::globalPhaseStorage
     */
    void globalPhaseStorage(PrimaryVariables &dest, int phaseIdx)
    {
        dest = 0;

        ElementIterator elemIt = this->gridView_().template begin<0>();
        const ElementIterator elemEndIt = this->gridView_().template end<0>();
        for (; elemIt != elemEndIt; ++elemIt)
        {
            this->localResidual().evalPhaseStorage(*elemIt, phaseIdx);
            for (int i = 0; i < elemIt->template count<dim>(); ++i)
                dest += this->localResidual().residual(i);
        };

        this->gridView_().comm().sum(dest);
    }

//    /*!
//     * \copydoc Implicit::relativeErrorDof
//     */
//    Scalar relativeErrorDof(const int dofIdx,
//                            const PrimaryVariables &priVars1,
//                            const PrimaryVariables &priVars2)
//    {
//        Scalar result = 0.0;
//        for (int j = 0; j < numEq; ++j) {
////            Scalar eqErr = std::log(std::abs(priVars1[j] - priVars2[j]));
//            Scalar eqErr = std::abs(priVars1[j] - priVars2[j]);
//            eqErr /= std::max<Scalar>(1.0, std::abs(priVars1[j] + priVars2[j])/2);
////            Scalar refValue = this->problem_().refValue(j);
////            eqErr /= std::max<Scalar>(std::min<Scalar>(1.0, refValue), std::abs(priVars1[j] + priVars2[j])/2);
////              eqErr /= std::abs(priVars1[j] + priVars2[j])/2;
//            result = std::max(result, eqErr);
//        }
//        return result;
//    }


//     /*!
//      * \copydoc Implicit::checkPlausibility
//      */
//     void checkPlausibility() const
//     {
//     	 // Looping over all elements of the domain
//     	        ElementIterator eEndIt = this->problem_().gridView().template end<0>();
//     	        for (ElementIterator eIt = this->problem_().gridView().template begin<0>() ; eIt not_eq eEndIt; ++eIt)
//     	        {
//     	            ElementVolumeVariables elemVolVars;
//     	            FVElementGeometry fvGeometry;
//
//     	            // updating the volume variables
//     	            fvGeometry.update(this->problem_().gridView(), *eIt);
//     	            elemVolVars.update(this->problem_(), *eIt, fvGeometry, false);
//
//     	            std::stringstream  message ;
//     	            // number of scv
//     	            const unsigned int numScv = fvGeometry.numScv; // box: numSCV, cc:1
//
//     	            for (unsigned int scvIdx = 0; scvIdx < numScv; ++scvIdx) {
//
//     	                const FluidState & fluidState = elemVolVars[scvIdx].fluidState();
//
//     	                // mass Check
//     	                const Scalar eps = plausibilityTolerance_ ;
//     	                for (int compIdx=0; compIdx< numComponents+numSecComponents; ++ compIdx){
//     	                    const Scalar xTest = elemVolVars[scvIdx].moleFraction(wPhaseIdx, compIdx);
//
//     	                    if (not std::isfinite(xTest) or xTest < 0.-eps or xTest > 1.+eps ){
//     	                        message <<"\nUnphysical Value in Mass: \n";
//
//     	                        message << "\tx" <<"_w"
//     	                                <<"^"<<FluidSystem::componentName(compIdx)<<"="
//     	                                << elemVolVars[scvIdx].moleFraction(wPhaseIdx, compIdx) <<"\n";
//     	                    }
//     	                }
//
//     	                // check chemistry by pH
//     	                Scalar pH = elemVolVars[scvIdx].pH();
//     	                if (not std::isfinite(pH) or pH < pHMin_ or pH > pHMax_ ){
//     	                    	                        message <<"\nUnphysical Value in pH: \n";
//
//     	                    	                        message << "pH = " << elemVolVars[scvIdx].pH() <<"\n";
//     	                }
//
//     	                // Some check wrote into the error-message, add some additional information and throw
//     	                if (not message.str().empty()){
//     	                    // Getting the spatial coordinate
//     	                    const GlobalPosition & globalPosCurrent = fvGeometry.subContVol[scvIdx].global;
//     	                    std::stringstream positionString ;
//
//     	                    // Add physical location
//     	                    positionString << "Here:";
//     	                    for(int i=0; i<dim; i++)
//     	                        positionString << " x"<< (i+1) << "="  << globalPosCurrent[i] << " "   ;
//     	                    message << "Unphysical value found! \n" ;
//     	                    message << positionString.str() ;
//     	                    message << "\n";
//
//     	                    message << " Here come the primary Variables:" << "\n" ;
//     	                    for(unsigned int priVarIdx =0 ; priVarIdx<numEq; ++priVarIdx){
//     	                        message << "priVar[" << priVarIdx << "]=" << elemVolVars[scvIdx].priVar(priVarIdx) << "\n";
//     	                    }
//     	                    DUNE_THROW(NumericalProblem, message.str());
//     	                }
//     	            } // end scv-loop
//     	        } // end element loop
//
//
//     }

    /*!
     * \copydoc 2pncMin::updateFailed
     */
    void updateFailed()
    {
        ParentType::updateFailed();

        setSwitched_(false);
        resetPhasePresence_();
    };

    /*!
     * \copydoc 2pncMin::advanceTimeLevel
     */
    void advanceTimeLevel()
    {
        ParentType::advanceTimeLevel();

        // update the phase state
        updateOldPhasePresence_();
        setSwitched_(false);
    }

    /*!
     * \copydoc 2pncMin::switched
     */
    bool switched() const
    {
        return switchFlag_;
    }

    /*!
     * \copydoc 2pncMin::phasePresence
     */
    int phasePresence(int globalVertexIdx, bool oldSol) const
    {
        return oldSol ? staticDat_[globalVertexIdx].oldPhasePresence
                : staticDat_[globalVertexIdx].phasePresence;
    }

    /*!
     * \copydoc 2pncMin::addOutputVtkFields
     */
    template<class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol,
                            MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > ScalarField;
        typedef Dune::BlockVector<Dune::FieldVector<double, dim> > VectorField;

        // get the number of degrees of freedom
        unsigned numDofs = this->numDofs();

        // velocity output currently only works for the box discretization
        if (!isBox)
           velocityOutput_ = false;

        // create the required scalar fields
 //       unsigned numScv = this->problem_().gridView().size(dim);

		ScalarField *Sg   		   = writer.allocateManagedBuffer (numDofs);
		ScalarField *Sl    		   = writer.allocateManagedBuffer (numDofs);
		ScalarField *pg    		   = writer.allocateManagedBuffer (numDofs);
		ScalarField *pl 		   = writer.allocateManagedBuffer (numDofs);
		ScalarField *pc 		   = writer.allocateManagedBuffer (numDofs);
		ScalarField *rhoL		   = writer.allocateManagedBuffer (numDofs);
		ScalarField *rhoG 		   = writer.allocateManagedBuffer (numDofs);
		ScalarField *mobL		   = writer.allocateManagedBuffer (numDofs);
        ScalarField *mobG 		   = writer.allocateManagedBuffer (numDofs);
        ScalarField *phasePresence = writer.allocateManagedBuffer (numDofs);
        ScalarField *temperature   = writer.allocateManagedBuffer (numDofs);
        ScalarField *poro          = writer.allocateManagedBuffer (numDofs);
        ScalarField *boxVolume     = writer.allocateManagedBuffer (numDofs);
        ScalarField *potential     = writer.allocateManagedBuffer (numDofs);
#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
        ScalarField *pH    		   = writer.allocateManagedBuffer (numDofs);

        ScalarField *Omega    		   = writer.allocateManagedBuffer (numDofs);
//        ScalarField *Appa_Ksp 		   = writer.allocateManagedBuffer (numDofs);
//        ScalarField *rprec    		   = writer.allocateManagedBuffer (numDofs);
//        ScalarField *rurea    		   = writer.allocateManagedBuffer (numDofs);
////        ScalarField *mue    		   = writer.allocateManagedBuffer (numDofs);
#endif
        ScalarField *solidity[numSPhases] ;
        for (int phaseIdx = 0; phaseIdx < numSPhases; ++phaseIdx)
        {
        	solidity[phaseIdx]= writer.allocateManagedBuffer (numDofs);
        }
#ifdef DUMUX_BIOTRANSP_PROBLEM_HH
        ScalarField *massFraction[numPhases][numComponents];
        for (int i = 0; i < numPhases; ++i)
            for (int j = 0; j < numComponents; ++j)
                massFraction[i][j] = writer.allocateManagedBuffer (numDofs);

        ScalarField *molarity[numComponents];
        for (int j = 0; j < numComponents; ++j)
            molarity[j] = writer.allocateManagedBuffer (numDofs);
#endif

#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
        ScalarField *massFraction[numPhases][numComponents + numSecComponents];
        for (int i = 0; i < numPhases; ++i)
            for (int j = 0; j < numComponents + numSecComponents; ++j)
                massFraction[i][j] = writer.allocateManagedBuffer (numDofs);

        ScalarField *molarity[numComponents + numSecComponents];
        for (int j = 0; j < numComponents + numSecComponents ; ++j)
            molarity[j] = writer.allocateManagedBuffer (numDofs);
        ScalarField *EN = writer.allocateManagedBuffer (numDofs);

//        ScalarField *Da_urea = writer.allocateManagedBuffer (numDofs);
//		ScalarField *Da_prec = writer.allocateManagedBuffer (numDofs);
#endif
        ScalarField *intrinsicPerm = writer.allocateManagedBuffer (numDofs);
//        ScalarField *intrinsicPerm[dim];
//        for (int j = 0; j < dim; ++j) //Permeability only in main directions xx and yy
//            intrinsicPerm[j] = writer.allocateManagedBuffer (numDofs);

        *boxVolume = 0;

#ifdef VELOCITY_OUTPUT // check if velocity output is demanded

#warning potential memory leak is possible here if the reserved memory for z velocity is not set free
        ScalarField *velocityX = writer.allocateManagedBuffer (numDofs);
        ScalarField *velocityY = writer.allocateManagedBuffer (numDofs);
        ScalarField *velocityZ = writer.allocateManagedBuffer (numDofs);

        // initialize velocity fields
        Scalar boxSurface[numDofs];
        for (int i = 0; i < numDofs; ++i)
        {
            (*velocityX)[i] = 0;
            if (dim > 1)
            (*velocityY)[i] = 0;
            if (dim > 2)
            (*velocityZ)[i] = 0;
            boxSurface[i] = 0.0; // initialize the boundary surface of the fv-boxes

#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
//            (*Da_urea)[i] = 0.0;
//            (*Da_prec)[i] = 0.0;
#endif
        }
#endif

        unsigned numElements = this->gridView_().size(0);
        ScalarField *rank =
                writer.allocateManagedBuffer (numElements);

        FVElementGeometry fvGeometry;
        VolumeVariables volVars;

        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator elemEndIt = this->gridView_().template end<0>();
        for (; elemIt != elemEndIt; ++elemIt)
        {
        	if(elemIt->partitionType() == Dune::InteriorEntity)
        	{
				int idx = this->problem_().elementMapper().map(*elemIt);
				(*rank)[idx] = this->gridView_().comm().rank();
				fvGeometry.update(this->gridView_(), *elemIt);

				int numVerts = elemIt->template count<dim> ();
				for (int i = 0; i < numVerts; ++i)
				{
					int globalIdx = this->vertexMapper().map(*elemIt, i, dim);
					volVars.update(sol[globalIdx],
								   this->problem_(),
								   *elemIt,
								   fvGeometry,
								   i,
								   false);

					GlobalPosition globalPos = fvGeometry.subContVol[i].global;
					Scalar zmax = this->problem_().bBoxMax()[dim-1];
					(*Sg)[globalIdx]     		 = volVars.saturation(nPhaseIdx);
					(*Sl)[globalIdx]     		 = volVars.saturation(wPhaseIdx);
					(*pg)[globalIdx]  	 		 = volVars.pressure(nPhaseIdx);
					(*pl)[globalIdx]   	 		 = volVars.pressure(wPhaseIdx);
					(*pc)[globalIdx]	 		 = volVars.capillaryPressure();
					(*rhoL)[globalIdx]	 	 	 = volVars.fluidState().density(wPhaseIdx);
					(*rhoG)[globalIdx]	  		 = volVars.fluidState().density(nPhaseIdx);
					(*mobL)[globalIdx] 	 	     = volVars.mobility(wPhaseIdx);
					(*mobG)[globalIdx] 	 	     = volVars.mobility(nPhaseIdx);
					(*boxVolume)[globalIdx]		+= fvGeometry.subContVol[i].volume;
					(*poro)[globalIdx] 		     = volVars.porosity();
	#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
					(*pH)[globalIdx] 		     = volVars.pH();

					(*Omega)[globalIdx] 		     = volVars.Omega();
	//                (*Appa_Ksp)[globalIdx] 		     = volVars.Appa_KSP();
	//                (*rprec)[globalIdx] 		     = volVars.rprec();
	//                (*rurea)[globalIdx] 		     = volVars.rurea();
	////                (*mue)[globalIdx] 		     = volVars.mue();
	#endif
					for (int phaseIdx =  0; phaseIdx < numSPhases; ++phaseIdx)
					{
						(*solidity[phaseIdx])[globalIdx]= volVars.solidity(phaseIdx + numPhases);
					}
					(*temperature)[globalIdx] 	 = volVars.temperature();
					(*phasePresence)[globalIdx]  = staticDat_[globalIdx].phasePresence;
					(*potential)[globalIdx]      = (volVars.pressure(wPhaseIdx)-1e5)
													/volVars.fluidState().density(wPhaseIdx)
													/9.81
													- (zmax - globalPos[dim-1]);
	#ifdef DUMUX_BIOTRANSP_PROBLEM_HH
					for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
						for (int compIdx = 0; compIdx < numComponents; ++compIdx)
						{
							(*massFraction[phaseIdx][compIdx])[globalIdx]= volVars.fluidState().massFraction(phaseIdx,compIdx);

							Valgrind::CheckDefined((*massFraction[phaseIdx][compIdx])[globalIdx]);

						}
					for (int compIdx = 0; compIdx < numComponents; ++compIdx)
							(*molarity[compIdx])[globalIdx] = (volVars.fluidState().molarity(wPhaseIdx, compIdx));
	#endif
	#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
					for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
						for (int compIdx = 0; compIdx < numComponents + numSecComponents; ++compIdx)
						{
							(*massFraction[phaseIdx][compIdx])[globalIdx]= volVars.fluidState().massFraction(phaseIdx,compIdx);

							Valgrind::CheckDefined((*massFraction[phaseIdx][compIdx])[globalIdx]);

						}
					(*EN) [globalIdx] = 0.0;
					for (int compIdx = 0; compIdx < numComponents + numSecComponents; ++compIdx)
					{
							(*molarity[compIdx])[globalIdx] = (volVars.fluidState().molarity(wPhaseIdx, compIdx));
							(*EN) [globalIdx] += (volVars.fluidState().molarity(wPhaseIdx, compIdx)) * FluidSystem::charge(compIdx);
					}
	#endif


					const Tensor &Perm = this->problem_().spatialParams().intrinsicPermeability(*elemIt, fvGeometry, i);
					(*intrinsicPerm)[globalIdx] = Perm[0][0] * volVars.permFactor();
	//                (*intrinsicPerm)[globalIdx] = this->problem_().spatialParams().intrinsicPermeability(*elemIt, fvGeometry, i) * volVars.permFactor();
	//                for (int j = 0; j<dim; ++j)
	//                    (*intrinsicPerm[j])[globalIdx] = Perm[j][j] * volVars.permFactor();

	//#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
	//                if (volVars.moleFraction(wPhaseIdx,UreaIdx) > 0.)
	//                	(*Da_urea)[globalIdx] = volVars.rurea() /(volVars.molarDensity(wPhaseIdx)*volVars.moleFraction(wPhaseIdx,UreaIdx));
	//                else
	//                	(*Da_urea)[globalIdx] = volVars.rurea() /(1e-10);
	//                if (volVars.moleFraction(wPhaseIdx,CaIdx) > 0.)
	//                	(*Da_prec)[globalIdx] = volVars.rprec() /(volVars.molarDensity(wPhaseIdx)*volVars.moleFraction(wPhaseIdx,CaIdx));
	//                else
	//                	(*Da_prec)[globalIdx] = volVars.rprec() /(1e-10);
	//#endif
				};

	#ifdef VELOCITY_OUTPUT // check if velocity output is demanded
				// In the box method, the velocity is evaluated on the FE-Grid. However, to get an
				// average apparent velocity at the vertex, all contributing velocities have to be interpolated.
				GlobalPosition velocity(0.);
				ElementVolumeVariables elemVolVars;

				elemVolVars.update(this->problem_(),
								   *elemIt,
								   fvGeometry,
								   false /* isOldSol? */);

				// loop over the phases
				for (int faceIdx = 0; faceIdx< fvGeometry.numScvf; faceIdx++)
				{
					//prepare the flux calculations (set up and prepare geometry, FE gradients)
					FluxVariables fluxDat(this->problem_(),
									 *elemIt,
									 fvGeometry,
									 faceIdx,
									 elemVolVars);

					// choose phase of interest. Alternatively, a loop over all phases would be possible.
					int phaseIdx = wPhaseIdx;

					// get darcy velocity
					velocity = fluxDat.Kmvp(phaseIdx); // mind the sign: vDarcy = kf grad p

					// up+downstream mobility
					const VolumeVariables &up = elemVolVars[fluxDat.upstreamIdx(phaseIdx)];
					const VolumeVariables &dn = elemVolVars[fluxDat.downstreamIdx(phaseIdx)];
					Scalar scvfArea = fluxDat.face().normal.two_norm(); //get surface area to weight velocity at the IP with the surface area
					velocity *= (massUpwindWeight_*up.mobility(phaseIdx) + (1-massUpwindWeight_)*dn.mobility(phaseIdx))* scvfArea;

					int vertIIdx = this->problem_().vertexMapper().map(*elemIt,
							fluxDat.face().i,
							dim);
					int vertJIdx = this->problem_().vertexMapper().map(*elemIt,
							fluxDat.face().j,
							dim);
					// add surface area for weighting purposes
					boxSurface[vertIIdx] += scvfArea;
					boxSurface[vertJIdx] += scvfArea;

					// Add velocity to upstream and downstream vertex.
					// Beware: velocity has to be substracted because of the (wrong) sign of vDarcy

					(*velocityX)[vertIIdx] -= velocity[0];
					(*velocityX)[vertJIdx] -= velocity[0];
					if (dim >= 2)
					{
						(*velocityY)[vertIIdx] -= velocity[1];
						(*velocityY)[vertJIdx] -= velocity[1];
					}
					if (dim == 3)
					{
						(*velocityZ)[vertIIdx] -= velocity[2];
						(*velocityZ)[vertJIdx] -= velocity[2];
					}
				}
        	}
	#endif
        }

#ifdef VELOCITY_OUTPUT // check if velocity output is demanded
        // normalize the velocities at the vertices
        for (int i = 0; i < numDofs; ++i)
        {
            (*velocityX)[i] /= boxSurface[i];
            Scalar abs_v = sqrt((*velocityX)[i]*(*velocityX)[i]);
            if (dim >= 2)
            {
				(*velocityY)[i] /= boxSurface[i];
				abs_v = sqrt((*velocityX)[i]*(*velocityX)[i] + (*velocityY)[i]*(*velocityY)[i]);
            }
            if (dim == 3)
            {
            	(*velocityZ)[i] /= boxSurface[i];
            	abs_v = sqrt((*velocityX)[i]*(*velocityX)[i] + (*velocityY)[i]*(*velocityY)[i] + (*velocityZ)[i]*(*velocityZ)[i]);
            }
//#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
//            if (abs_v<1e-10 )
//            {
//            	(*Da_urea)[i] /= (1e-10);
//            	(*Da_prec)[i] /= (1e-10);
//            }
//            else
//            {
//            	(*Da_urea)[i] /= abs_v;
//            	(*Da_prec)[i] /= abs_v;
//            }
//#endif
        }
#endif

        writer.attachVertexData(*Sg, "Sg");
        writer.attachVertexData(*Sl, "Sl");
        writer.attachVertexData(*pg, "pg");
        writer.attachVertexData(*pl, "pl");
        writer.attachVertexData(*pc, "pc");
        writer.attachVertexData(*rhoL, "rhoL");
        writer.attachVertexData(*rhoG, "rhoG");
        writer.attachVertexData(*mobL, "mobL");
        writer.attachVertexData(*mobG, "mobG");
        writer.attachVertexData(*poro, "porosity");
        writer.attachVertexData(*temperature, "temperature");
        writer.attachVertexData(*phasePresence, "phase presence");
        writer.attachVertexData(*potential, "potential");
        writer.attachVertexData(*boxVolume, "boxVolume");
#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
        writer.attachVertexData(*pH, "pH");

        writer.attachVertexData(*Omega, "Omega");
//        writer.attachVertexData(*Appa_Ksp, "Apparent Calcite-Solubilityproduct");
//        writer.attachVertexData(*rprec, "rprec");
//        writer.attachVertexData(*rurea, "rurea");
//        writer.attachVertexData(*mue, "mue");
#endif
        for (int i = 0; i < numSPhases; ++i)
        {
                        std::ostringstream oss;
            oss << "Volume_Fraction_"
                << FluidSystem::componentName(numComponents + numSecComponents + i);
            writer.attachDofData(*solidity[i], oss.str().c_str(), isBox);
//            std::string name = (boost::format("Volume_Fraction_%s")
//                                % FluidSystem::componentName(numComponents + numSecComponents + phaseIdx)).str();		//TODO componentName(numComponents + numSecComponents -1 + phaseIdx)).str() for vishal
//            writer.attachVertexData(*solidity[phaseIdx], name.c_str());
        }
//        writer.attachVertexData(*NaClMolality, "MolalityNaCl");

        writer.attachVertexData(*intrinsicPerm, "Kxx");
//        writer.attachVertexData(*intrinsicPerm[0], "Kxx");
//        if (dim >= 2)
//            writer.attachVertexData(*intrinsicPerm[1], "Kyy");
//        if (dim == 3)
//            writer.attachVertexData(*intrinsicPerm[2], "Kzz");

#ifdef DUMUX_BIOTRANSP_PROBLEM_HH
        for (int i = 0; i < numPhases; ++i)
        {
            for (int j = 0; j < numComponents; ++j)
            {
            	std::ostringstream oss;
				oss << "X_"
				    << ((i == wPhaseIdx) ? "l" : "g")	//FluidSystem::phaseName(i)
					//<< "_"
					<< FluidSystem::componentName(j);
                writer.attachVertexData(*massFraction[i][j], oss.str().c_str());
//                std::string name = (boost::format("X_%s%s")
//                                    % ((i == wPhaseIdx) ? "l" : "g")
//                                    % FluidSystem::componentName(j)).str();
//                writer.attachVertexData(*massFraction[i][j], name.c_str());
            }
        }

        for (int j = 0; j < numComponents; ++j)
        {
            std::ostringstream oss;
			oss << "Molarity_"
				<< FluidSystem::componentName(j);
			writer.attachVertexData(*molarity[j], oss.str().c_str());
//            std::string name = (boost::format("Molarity_%s")
//                    % FluidSystem::componentName(j)).str();
//            writer.attachVertexData(*molarity[j], name.c_str());
        }
#endif
#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
        for (int i = 0; i < numPhases; ++i)
        {
            for (int j = 0; j < numComponents + numSecComponents; ++j)
            {
            	std::ostringstream oss;
				oss << "X_"
				    << ((i == wPhaseIdx) ? "l" : "g")	//FluidSystem::phaseName(i)
					//<< "_"
					<< FluidSystem::componentName(j);
                writer.attachVertexData(*massFraction[i][j], oss.str().c_str());
//                std::string name = (boost::format("X_%s%s")
//                                    % ((i == wPhaseIdx) ? "l" : "g")
//                                    % FluidSystem::componentName(j)).str();
//                writer.attachVertexData(*massFraction[i][j], name.c_str());
            }
        }

        for (int j = 0; j < numComponents + numSecComponents; ++j)
        {
            std::ostringstream oss;
			oss << "Molarity_"
				<< FluidSystem::componentName(j);
			writer.attachVertexData(*molarity[j], oss.str().c_str());
//            std::string name = (boost::format("Molarity_%s")
//                    % FluidSystem::componentName(j)).str();
//            writer.attachVertexData(*molarity[j], name.c_str());
        }
        writer.attachVertexData(*EN, "Sum_charges");
#endif
//#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
//        writer.attachVertexData(*Da_urea, "Da_ureolysis");
//        writer.attachVertexData(*Da_prec, "Da_precipitation");
//#endif

#ifdef VELOCITY_OUTPUT // check if velocity output is demanded
        writer.attachVertexData(*velocityX, "Vx");
        if (dim >= 2)
        writer.attachVertexData(*velocityY, "Vy");
        if (dim == 3)
        writer.attachVertexData(*velocityZ, "Vz");

#endif
        writer.attachCellData(*rank, "process rank");
    }

    /*!
     * \brief Write the current solution to a restart file.
     *
     * \param outStream The output stream of one vertex for the restart file
     * \param entity The Entity
     */
    template<class Entity>
    void serializeEntity(std::ostream &outStream, const Entity &entity)
    {
        // write primary variables
        ParentType::serializeEntity(outStream, entity);

        int vertIdx = this->dofMapper().map(entity);
        if (!outStream.good())
            DUNE_THROW(Dune::IOError, "Could not serialize vertex " << vertIdx);

        outStream << staticDat_[vertIdx].phasePresence << " ";
    }

    /*!
     * \brief Reads the current solution for a vertex from a restart
     *        file.
     *
     * \param inStream The input stream of one vertex from the restart file
     * \param entity The Entity
     */
    template<class Entity>
    void deserializeEntity(std::istream &inStream, const Entity &entity)
    {
        // read primary variables
        ParentType::deserializeEntity(inStream, entity);

        // read phase presence
        int vertIdx = this->dofMapper().map(entity);
        if (!inStream.good())
            DUNE_THROW(Dune::IOError,
                       "Could not deserialize vertex " << vertIdx);

        inStream >> staticDat_[vertIdx].phasePresence;
        staticDat_[vertIdx].oldPhasePresence
                = staticDat_[vertIdx].phasePresence;

    }

    /*!
     * \copydoc 2pncMin::updateStaticData
     */
    void updateStaticData(SolutionVector &curGlobalSol,
                          const SolutionVector &oldGlobalSol)
    {
        bool wasSwitched = false;

        for (unsigned i = 0; i < staticDat_.size(); ++i)
            staticDat_[i].visited = false;

        FVElementGeometry fvGeometry;
        static VolumeVariables volVars;
        ElementIterator it = this->gridView_().template begin<0> ();
        const ElementIterator &endit = this->gridView_().template end<0> ();
        for (; it != endit; ++it)
        {
            fvGeometry.update(this->gridView_(), *it);
            for (int i = 0; i < fvGeometry.numScv; ++i)
            {
                int globalIdx = this->vertexMapper().map(*it, i, dim);

                if (staticDat_[globalIdx].visited)
                    continue;

                staticDat_[globalIdx].visited = true;
                volVars.update(curGlobalSol[globalIdx],
                               this->problem_(),
                               *it,
                               fvGeometry,
                               i,
                               false);
                const GlobalPosition &global = it->geometry().corner(i);
                if (primaryVarSwitch_(curGlobalSol,
                                      volVars,
                                      globalIdx,
                                      global))
                    wasSwitched = true;
            }
        }

        // make sure that if there was a variable switch in an
        // other partition we will also set the switch flag
        // for our partition.
        wasSwitched = this->gridView_().comm().max(wasSwitched);

        setSwitched_(wasSwitched);
    }

protected:
    /*!
     * \copydoc 2pncMin::StaticVars
     */
    struct StaticVars
    {
        int phasePresence;
        bool wasSwitched;

        int oldPhasePresence;
        bool visited;
    };

    /*!
     * \copydoc 2pncMin::resetPhasePresence_
     */
    void resetPhasePresence_()
    {
        int numDofs = this->gridView_().size(dim);
        for (int i = 0; i < numDofs; ++i)
        {
            staticDat_[i].phasePresence
                    = staticDat_[i].oldPhasePresence;
            staticDat_[i].wasSwitched = false;
        }
    }

    /*!
     * \copydoc 2pncMin::updateOldPhasePresence_
     */
    void updateOldPhasePresence_()
    {
        int numDofs = this->gridView_().size(dim);
        for (int i = 0; i < numDofs; ++i)
        {
            staticDat_[i].oldPhasePresence
                    = staticDat_[i].phasePresence;
            staticDat_[i].wasSwitched = false;
        }
    }

    /*!
     * \copydoc 2pncMin::setSwitched_
     */
    void setSwitched_(bool yesno)
    {
        switchFlag_ = yesno;
    }

    //  perform variable switch at a vertex; Returns true if a
    //  variable switch was performed.
    bool primaryVarSwitch_(SolutionVector &globalSol,
                           const VolumeVariables &volVars, int globalIdx,
                           const GlobalPosition &globalPos)
    {
        // evaluate primary variable switch
        bool wouldSwitch = false;
        int phasePresence = staticDat_[globalIdx].phasePresence;
        int newPhasePresence = phasePresence;

        // check if a primary var switch is necessary
        if (phasePresence == nPhaseOnly)
        {
            // calculate mole fraction in the hypothetic liquid phase
//            Scalar xll = volVars.fluidState().moleFraction(wPhaseIdx, wCompIdx);
//            Scalar xlg = volVars.fluidState().moleFraction(wPhaseIdx, nCompIdx);
//            Scalar xlNaCl = volVars.fluidState().moleFraction(wPhaseIdx, NaClIdx);

            Scalar xlMax = 1.0;
            Scalar sumxl  = 0;
            for(int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
            	sumxl += volVars.fluidState().moleFraction(wPhaseIdx, compIdx);
            }

            if (sumxl > xlMax)
                wouldSwitch = true;
            if (staticDat_[globalIdx].wasSwitched)
                xlMax *= 1.02;

            // if the sum of the mole fractions would be larger than
            // 100%, liquid phase appears
            if (sumxl > xlMax)
            {
                // liquid phase appears
                std::cout << "liquid phase appears at vertex " << globalIdx
                        << ", coordinates: " << globalPos << ", sum of xl: "
                        << sumxl << std::endl;
                newPhasePresence = bothPhases;
                if (formulation == pgSl)
                    globalSol[globalIdx][switchIdx] = 0.0;
                else if (formulation == plSg)
                    globalSol[globalIdx][switchIdx] = 1.0;
            };
        }
        else if (phasePresence == wPhaseOnly)
        {
            // calculate fractions of the partial pressures in the
            // hypothetic gas phase
//            Scalar xgl = volVars.fluidState().moleFraction(nPhaseIdx, wCompIdx);
//            Scalar xgg = volVars.fluidState().moleFraction(nPhaseIdx, nCompIdx);
//            Scalar xgNaCl = volVars.fluidState().moleFraction(nPhaseIdx, NaClIdx);

            Scalar xgMax = 1.0;
            Scalar sumxg  = 0;
            for(int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
            	sumxg += volVars.fluidState().moleFraction(nPhaseIdx, compIdx);
            }
            if (sumxg > xgMax)
                wouldSwitch = true;
            if (staticDat_[globalIdx].wasSwitched)
                xgMax *= 1.02;

            // if the sum of the mole fractions would be larger than
            // 100%, gas phase appears
            if (sumxg > xgMax)
            {
                // gas phase appears
                std::cout << "gas phase appears at vertex " << globalIdx
                        << ", coordinates: " << globalPos << ", sum of xg: "
                        << sumxg << std::endl;
                newPhasePresence = bothPhases;
                if (formulation == pgSl)
                    globalSol[globalIdx][switchIdx] = 0.999;
                else if (formulation == plSg)
                    globalSol[globalIdx][switchIdx] = 0.001;
            }
        }
        else if (phasePresence == bothPhases)
        {
            Scalar Smin = 0.0;
            if (staticDat_[globalIdx].wasSwitched)
                Smin = -0.01;

            if (volVars.saturation(nPhaseIdx) <= Smin)
            {
                wouldSwitch = true;
                // gas phase disappears
                std::cout << "Gas phase disappears at vertex " << globalIdx
                        << ", coordinates: " << globalPos << ", Sg: "
                        << volVars.saturation(nPhaseIdx) << std::endl;
                newPhasePresence = wPhaseOnly;

                globalSol[globalIdx][switchIdx]
                        = volVars.fluidState().moleFraction(wPhaseIdx, nCompIdx);
            }

            else if (volVars.saturation(wPhaseIdx) <= Smin)
            {
                wouldSwitch = true;
                // liquid phase disappears
                std::cout << "Liquid phase disappears at vertex " << globalIdx
                        << ", coordinates: " << globalPos << ", Sl: "
                        << volVars.saturation(wPhaseIdx) << std::endl;
                newPhasePresence = nPhaseOnly;

                globalSol[globalIdx][switchIdx]
                        = volVars.fluidState().moleFraction(nPhaseIdx, wCompIdx);
            }
        }

        staticDat_[globalIdx].phasePresence = newPhasePresence;
        staticDat_[globalIdx].wasSwitched = wouldSwitch;
        return phasePresence != newPhasePresence;
    }

    // parameters given in constructor
    std::vector<StaticVars> staticDat_;
    bool switchFlag_;
    bool velocityOutput_;

    Scalar plausibilityTolerance_;
    Scalar pHMax_;
    Scalar pHMin_;
    Scalar massUpwindWeight_;
};

}

#include "dumux/implicit/2pncmin/2pncminpropertydefaults.hh"

#endif
