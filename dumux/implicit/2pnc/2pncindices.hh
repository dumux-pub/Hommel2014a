// $Id: 2p2cproperties.hh 3784 2010-06-24 13:43:57Z bernd $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \brief Defines the indices required for the 2pncMin BOX model.
 */
#ifndef DUMUX_2PNC_INDICES_HH
#define DUMUX_2PNC_INDICES_HH
#include "2pncproperties.hh"
namespace Dumux
{
/*!
 * \ingroup TwoPNCModel
 */
// \{

/*!
 * \brief Enumerates the formulations which the 2p2c model accepts.
 */
struct TwoPNCFormulation
{
    enum {
        plSg,
        pgSl,
        pnSw = pgSl,
        pwSn = plSg
    };
};

/*!
 * \brief The indices for the isothermal TwoPNC model.
 *
 * \tparam formulation The formulation, either pwSn or pnSw.
 * \tparam PVOffset The first index in a primary variable vector.
 */
template <class TypeTag, int PVOffset = 0>
class TwoPNCIndices
{
	typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

public:
	// Phase indices
    static const int wPhaseIdx = FluidSystem::wPhaseIdx; //!< Index of the wetting phase
    static const int nPhaseIdx = FluidSystem::nPhaseIdx; //!< Index of the non-wetting phase
    // present phases (-> 'pseudo' primary variable)
    static const int wPhaseOnly = 1; //!< Only the non-wetting phase is present
    static const int nPhaseOnly = 0; //!< Only the wetting phase is present
    static const int bothPhases = 2; //!< Both phases are present

    // Primary variable indices
    static const int pressureIdx = PVOffset + 0; //!< Index for wetting/non-wetting phase pressure (depending on formulation) in a solution vector
    static const int switchIdx = PVOffset + 1; //!< Index of the either the saturation or the mass fraction of the non-wetting/wetting phase
    // equation indices
    static const int conti0EqIdx = PVOffset + 0; //!< Reference index for mass conservation equations.
    static const int contiWEqIdx = conti0EqIdx + FluidSystem::wCompIdx; //!< Index of the mass conservation equation for the wetting phase major component
    static const int contiNEqIdx = conti0EqIdx + FluidSystem::nCompIdx; //!< Index of the mass conservation equation for the non-wetting phase major component
};

// \}

}

#endif
