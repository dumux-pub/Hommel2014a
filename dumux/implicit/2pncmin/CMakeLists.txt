
install(FILES
        2pncminfluxvariables.hh
        2pncminindices.hh
        2pncminlocalresidual.hh
        2pncminmodel.hh
        2pncminproperties.hh
        2pncminpropertydefaults.hh
        2pncminvolumevariables.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/implicit/2pncmin)
