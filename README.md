Summary
=======

Hommel2014a contains a fully working version of the DuMuX code used
for model improvement and recalibration by inverse modeling as
published in WRR.
The required DUNE modules are listed at the end of this README.

The Results folder contains the data used to generate the plots (in .vtu and .mat format)

The experimental results are not included here, since they will be published
in separate publications.
The iTOUGH2 inputfiles are excluded for the same reason.
To get the experimental data, please contact

Ellen Lauchnor  for any of the column experiment data
ellen.lauchnor@biofilm.montana.edu
for any of the column experiment data

or
Adrienne Phillips
adrienne.phillips@biofilm.montana.edu
for the bicycle rim experiment data

Bibtex entry:
@ARTICLE{Hommel2015,
  author = {Hommel, Johannes and Lauchnor, Ellen G. and Phillips, Adrienne J.
and Gerlach, Robin and Cunningham, Alfred B. and Helmig, Rainer and
Ebigbo, Anozie and Class, Holger},
  title = {{A revised model for microbially induced calcite precipitation: Improvements
and new insights based on recent experiments}},
  journal = {Water Resources Research},
  year = {2015},
  volume = {51},
  pages = {3695-3715},
  number = {5},
  month = {May},
  doi = {10.1002/2014WR016503},
  owner = {hommel},
  timestamp = {2014.11.05}
}

Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installHommel2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2014a/raw/master/installHommel2014a.sh)
in this folder.

```bash
mkdir -p Hommel2014a && cd Hommel2014a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2014a/raw/master/installHommel2014a.sh
sh ./installHommel2014a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/co2/biomin/,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/co2/biomin/biomin.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.


Used Versions and Software
==========================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

After some changes (see commit 37264178e882902adb42ec223bdf7d3ce7038cad),
this dumux-pub module is now working with dune-release branch 2.4 and
at least dumux release version 2.6.

dumux...................: version 2.5 (/temp/hommel/DUMUX_aktuell/dumux)
dune-common.............: version 2.3 (/temp/hommel/DUMUX_aktuell/dune-common)
dune-geometry...........: version 2.3 (/temp/hommel/DUMUX_aktuell/dune-geometry)
dune-grid...............: version 2.3 (/temp/hommel/DUMUX_aktuell/dune-grid)
dune-istl...............: version 2.3 (/temp/hommel/DUMUX_aktuell/dune-istl)
dune-localfunctions.....: version 2.3 (/temp/hommel/DUMUX_aktuell/dune-localfunctions)
dune-pdelab.............: no (/temp/hommel/DUMUX_aktuell/dune-pdelab)
ALBERTA.................: no
ALUGrid.................: no
AlgLib for DUNE.........: no
AmiraMesh...............: no
BLAS....................: yes
GMP.....................: yes
Grape...................: no
METIS...................: no
MPI.....................: no (OpenMPI)
OpenGL..................: yes (add GL_LIBS to LDADD manually, etc.)
ParMETIS................: no
SuperLU-DIST............: no
SuperLU.................: yes (version 4.3 or newer)
UG......................: yes (sequential)
psurface................: no
