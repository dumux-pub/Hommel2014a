% ========================================================================
% Arbitraty Integrative Probabilistic Collocation Method
% Author: Dr. Sergey Oladyshkin
% SRC SimTech,Universitaet Stuttgart, Pfaffenwaldring 61, 70569 Stuttgart
% E-mail: Sergey.Oladyshkin@iws.uni-stuttgart.de
% ========================================================================
%modified by Lena Walter 
clear all;

%-------------------------------------------------------------------------------------------
%---------- Reading VTU's
%-------------------------------------------------------------------------------------------

               
%         %--->Reading of Names for Simulation Output Files        
        fid = fopen('Column.pvd', 'r');
        i=0;
        while feof(fid) == 0
            line = fgetl(fid);
            if (findstr(line,'DataSet')~=0) 
                i=i+1;                
                ref_Time(i)=str2num(line(findstr(line,'timestep="')+length('timestep="'):findstr(line,'" f')-1));
                ref_SimulationOutputFileNames{i}=line(findstr(line,'file="')+length('file="'):findstr(line,'"/>')-1);
            end
        end
        fclose(fid);
            
                %--->Reading Number of Points/Cells and Box/Cell-Volumes 
        fid = fopen(strcat(ref_SimulationOutputFileNames{1}), 'rt'); 
        while feof(fid) == 0
        line = fgetl(fid);
                if (findstr(line,'NumberOfPoints=')~=0)
                    ref_NumberOfPoints=str2num(line(findstr(line,'NumberOfPoints="')+length('NumberOfPoints="'):findstr(line,'">')-1));
                end
                if (findstr(line,'Name="boxVolume"')~=0)
                    ref_vol=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'NumberOfCells=')~=0)
                    ref_NumberOfCells=str2num(line(findstr(line,'NumberOfCells="')+length('NumberOfCells="'):findstr(line,'">')-1));
                end
                if (findstr(line,'Name="cell')~=0) & (~isempty(strfind(line,'volume')~=0))
                    ref_cellVolume=fscanf(fid, '%g', [1 ref_NumberOfCells]); 
                end
                
        end
        fclose(fid);
        
        %---> Reading from all Output Simulation Files
        for j=1:1:length(ref_SimulationOutputFileNames)
            fid = fopen(strcat(ref_SimulationOutputFileNames{j}), 'rt');
            i=0;
            while feof(fid) == 0   
                i=i+1;
                line = fgetl(fid);
                if (findstr(line,'Name="mobG"')~=0)
                   ref_mobG(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="mobL"')~=0)
                   ref_mobL(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="pc"')~=0)
                   ref_pc(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="pg"')~=0)
                   ref_pg(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="Porosity"')~=0)
                   ref_porosity(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end                 
		if (findstr(line,'Name="porosity"')~=0)
                   ref_porosity(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="temperature"')~=0)
                   ref_temperature(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="Sg"')~=0)
                   ref_Sg(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="X_gBrine"')~=0)
                   ref_x_gbrine(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="X_lBrine"')~=0)
                   ref_x_lbrine(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="X_gCO2"')~=0)
                   ref_x_gco2(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="X_lCO2"')~=0)
                   ref_x_lco2(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end                       
                if (findstr(line,'Name="rhoG"')~=0)
                    ref_rhoG(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end  
                if (findstr(line,'Name="rhoL"')~=0)
                    ref_rhoL(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Sl"')~=0)
                    ref_Sl(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end   
%                 if (strfind(line,'Name="plume')~=0) & (~isempty(strfind(line,'boundary')~=0))
%                    ref_plumeBoundary(j,:)=fscanf(fid, '%g', [1 ref_NumberOfCells]); 
%                 end 
%                 if (strfind(line,'Name="plume')~=0) & (~isempty(strfind(line,'volume')~=0))
%                    ref_plumeVolume(j,:)=fscanf(fid, '%g', [1 ref_NumberOfCells]); 
%                 end
%                 if (findstr(line,'Name="velocityG"')~=0)
%                    ref_velocityG(j,:)=fscanf(fid, '%g', [1 ref_NumberOfCells]); 
%                 end
%                 if (findstr(line,'Name="velocityL"')~=0)
%                    ref_velocityL(j,:)=fscanf(fid, '%g', [1 ref_NumberOfCells]); 
%                 end
                if (findstr(line,'Name="viscoL"')~=0)
                   ref_viscoL(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="viscoG"')~=0)
                   ref_viscoG(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="pH"')~=0)
                   ref_pH(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
%                if (findstr(line,'Name="Omega Pitzer"')~=0)
 %                  ref_omega(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
  %              end
                if (findstr(line,'Name="Salinity"')~=0)
                   ref_salinity(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="X_lCTot"')~=0)
                   ref_X_lCTot(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end                 
                if (findstr(line,'Name="X_lHCO3"')~=0)
                   ref_X_lHCO3(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="X_lCO3"')~=0)
                   ref_X_lCO3(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="X_lCa"')~=0)
                   ref_X_lCa(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="X_lH"')~=0)
                   ref_X_lH(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="X_lOH"')~=0)
                   ref_X_lOH(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="X_lNa"')~=0)
                   ref_X_lNa(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="X_lCl"')~=0)
                   ref_X_lCl(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Volume_Fraction_Calcite"')~=0)
                   ref_Porosity_Calcite(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
% 
		if (findstr(line,'Name="Volume_Fraction_Biofilm"')~=0)
                   ref_Porosity_Biofilm(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
		if (findstr(line,'Name="X_lNH3"')~=0)
                   ref_X_l_NH3(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
		if  (findstr(line,'Name="X_lNH4"')~=0)
                   ref_X_l_NH4(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
               	if (findstr(line,'Name="X_lUrea"')~=0)
                   ref_X_lUrea(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
		if (findstr(line,'Name="X_lO2"')~=0)
                   ref_X_lO2(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
		if (findstr(line,'Name="X_gO2"')~=0)
                   ref_X_gO2(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
		if (findstr(line,'Name="X_lSuspended"')~=0) & (~isempty(findstr(line,'Name="X_lBiomass"')~=0))
                   ref_X_lSuspended_Biomass(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
		if (findstr(line,'Name="X_lSubstrate"')~=0)
                   ref_X_lSubstrate(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
		if (findstr(line,'Name="Permeability"')~=0)
                   ref_Permeability(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
        end 
		if (findstr(line,'Name="Kxx"')~=0)
                   ref_Permeability(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]);
        end 
                

%
                if (findstr(line,'Name="Coordinates"')~=0)
                   ref_Coordinates(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints*3]); 
                end
                if (findstr(line,'Name="Molarity_TotalC"')~=0)
                   ref_MolarityCTot(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end                 
                if (findstr(line,'Name="Molarity_HCO3"')~=0)
                   ref_MolarityHCO3(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end 
                if (findstr(line,'Name="Molarity_CO3"')~=0)
                   ref_MolarityCO3(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Molarity_CO2"')~=0)
                   ref_MolarityCO2(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Molarity_Ca"')~=0)
                   ref_MolarityCa(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Molarity_H"')~=0)
                   ref_MolarityH(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Molarity_OH"')~=0)
                   ref_MolarityOH(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Molarity_Na"')~=0)
                   ref_MolarityNa(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Molarity_Cl"')~=0)
                   ref_MolarityCl(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                                
                if (findstr(line,'Name="Molarity_TotalNH"')~=0)
                   ref_MolarityTNH(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Molarity_NH4"')~=0)
                   ref_MolarityNH4(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="rprec"')~=0)
                   ref_rprec(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="rurea"')~=0)
                   ref_rurea(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Omega"')~=0)
                   ref_Omega(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end
                if (findstr(line,'Name="Appa_Ksp"')~=0)
                   ref_Appa_Ksp(j,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints]); 
                end               
            end
            fclose(fid);  
        
        end
        
save('ref.mat');

    
